#ifndef _ISLETSPACEIOT_H_
#define _ISLETSPACEIOT_H_

#ifndef ENABLE
#define ENABLE 1
#endif

#ifndef DISABLE
#define DISABLE 0
#endif

#ifndef LOW_LEVEL
#define LOW_LEVEL 0U
#endif

#ifndef HIGH_LEVEL
#define HIGH_LEVEL 1U
#endif

#endif