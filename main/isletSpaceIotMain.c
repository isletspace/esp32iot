
/*------------------------------------------------------------------------------------
 * HEADER FILES
 *-----------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>

#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_event.h"
#include "esp_netif.h"

#include "nvs_flash.h"
#include "protocol_examples_common.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"    // added in 20211229

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"
#include "lwip/err.h"    // added in 20211229
#include "lwip/sys.h"    // added in 20211229

#include "driver/gpio.h" // added in 20211229

#include "mqtt_client.h"

#include "isletSpaceIot.h"

#include "event_source.h"  // added in 20211229

/*------------------------------------------------------------------------------------
 * GLOBAL DEFINITIONS
 *-----------------------------------------------------------------------------------*/
#define    WIFI_SSID             CONFIG_EXAMPLE_WIFI_SSID      // WiFi名称
#define    WIFI_PASS             CONFIG_EXAMPLE_WIFI_PASSWORD  // WiFi密码
#define    WIFI_MAXIMUM_RETRY    100                      // 最大重连次数

/* The event group allows multiple bits for each event, but we only care about two events:
 * - we are connected to the AP with an IP
 * - we failed to connect after the maximum amount of retries */
#define    WIFI_CONNECTED_BIT    BIT0
#define    WIFI_FAIL_BIT         BIT1


#define    MQTT_TOPIC_ISLETSPACE_IOT_CTRL       "ISLETSPACE_IOT_FISHBOX_01_CTRL"  // 自定义订阅和发布的主题
#define    MQTT_TOPIC_ISLETSPACE_IOT_STATUS       "ISLETSPACE_IOT_FISHBOX_01_STATUS"  // 自定义订阅和发布的主题
#define    MQTT_INIT_MSG_ISLETSPACE_IOT    "isletSpaceIot-01 mqtt service is initialized and connected"  // 初始化成功后会向服务器发送的数据


// 引脚定义
#define    GPIO_ISLET_HEAT     14    // 加热棒控制引脚
#define    GPIO_ISLET_PUMP     27    // 水泵控制引脚
#define    GPIO_ISLET_LIGHT    26    // 灯光控制引脚
#define    GPIO_ISLET_OUTPUT     ((1ULL<<GPIO_ISLET_HEAT) | (1ULL<<GPIO_ISLET_PUMP) | (1ULL<<GPIO_ISLET_LIGHT))    // 将所有输出进行或运算，方便后面一起设置

#define    GPIO_ISLET_TEMP1    12    // 温度传感器
#define    GPIO_ISLET_TEMP2    13    // 温度传感器
#define    GPIO_ISLET_INPUT     ((1ULL<<GPIO_ISLET_TEMP1) | (1ULL<<GPIO_ISLET_TEMP2) )    // 将所有需要相同设置的输入引脚进行或运算，方便后面一起设置


#define    ESP_INTR_FLAG_DEFAULT 0


// 以下为DHT11传感器的相关宏定义

#define    DHT11_CLR(x)     {gpio_set_level(x, 0);}
#define    DHT11_SET(x)     {gpio_set_level(x, 1);}
#define    DHT11_IN(x)      {gpio_set_direction(x, GPIO_MODE_INPUT);}
#define    DHT11_OUT(x)     {gpio_set_direction(x, GPIO_MODE_OUTPUT);}




/*------------------------------------------------------------------------------------
 * ENUM
 *-----------------------------------------------------------------------------------*/
/*
QoS 是消息的发送方（Sender）和接受方（Receiver）之间达成的一个协议：
- QoS0 代表:
    Sender 发送的一条消息，Receiver 最多能收到一次，也就是说 Sender 尽力向 Receiver 发送消息，如果发送失败，也就算了；
- QoS1 代表:
    Sender 发送的一条消息，Receiver 至少能收到一次，也就是说 Sender 向 Receiver 发送消息，如果发送失败，会继续重试，直到 Receiver 收到消息为止，但是因为重传的原因，Receiver 有可能会收到重复的消息；
- QoS2 代表:
    Sender 发送的一条消息，Receiver 确保能收到而且只收到一次，也就是说 Sender 尽力向 Receiver 发送消息，如果发送失败，会继续重试，直到 Receiver 收到消息为止，同时保证 Receiver 不会因为消息重传而收到重复的消息。
*/
enum mqtt_qos{
    QOS0 = 0,
    QOS1,
    QOS2
};

enum mqtt_data_subscript{
    gpio_heat = 1,
    gpio_pump,
    gpio_light
};


/*------------------------------------------------------------------------------------
 * GLOBAL PARAMETERS DECLARATION
 *-----------------------------------------------------------------------------------*/
/* FreeRTOS event group to signal when we are connected*/
static EventGroupHandle_t s_wifi_event_group;  // 连接时需要使用到的FreeRTOS事件组
static const char *TAG = "ISLETSPACEIOT";
static int s_retry_num = 0;

// 以下为DHT11传感器数据的存储变量
uint8_t DHT11Data[4]={0};
uint8_t temperature, humidity;






/*------------------------------------------------------------------------------------
 * FUNCTIONS DECLARATION
 *-----------------------------------------------------------------------------------*/
static void logErrorIfNonzero(const char*, int);

void wifiStart(void);
static void wifiEventHandler(void*, esp_event_base_t, int32_t , void* );
static esp_err_t mqttEventHandlerCb(esp_mqtt_event_handle_t);

static void mqttStart(void);
static void mqttEventHandler(void *, esp_event_base_t, int32_t , void *);

void dht11Start(void);
void dht11Init(int);
uint8_t DHT11ReadValue(int);
uint8_t DHT11ReadTemHum(uint8_t* ,int);


static void gpioInit();
void DelayUs(uint32_t);



// 创建DHT11任务
// static void taskDHT11(void);  // added in 20211229

/*------------------------------------------------------------------------------------
 * MAIN FUNCTION
 *-----------------------------------------------------------------------------------*/
void app_main(void)
{
    // 启动并提示用户剩余堆大小 及 idf版本号
    ESP_LOGI(TAG, "[APP] Startup..");
    ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "[APP] IDF version: %s", esp_get_idf_version());

    esp_log_level_set("*", ESP_LOG_INFO);
    esp_log_level_set("MQTT_CLIENT", ESP_LOG_VERBOSE); 
    esp_log_level_set("MQTT_EXAMPLE", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_TCP", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_SSL", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT", ESP_LOG_VERBOSE);
    esp_log_level_set("OUTBOX", ESP_LOG_VERBOSE);

    gpioInit();                           // 初始化GPIO

    ESP_ERROR_CHECK(esp_event_loop_create_default());  // 创建默认的事件循环

    ESP_ERROR_CHECK(nvs_flash_init());    // nvs-flash 初始化
    ESP_LOGI(TAG, "ESP_WIFI_MODE_START");
    wifiStart();                          // WiFi-Station 模式初始化
    ESP_LOGI(TAG, "DHT11_START");
    dht11Start();                         // dht11 启动
    ESP_LOGI(TAG, "ESP_MQTT_START");
    mqttStart();                          // mqtt-tcp 模式启动




}


/*------------------------------------------------------------------------------------
 * FUNCTIONS DEFINITION
 *-----------------------------------------------------------------------------------*/
static void logErrorIfNonzero(const char * message, int error_code){
    if (error_code != 0) {
        ESP_LOGE(TAG, "Last error %s: 0x%x", message, error_code);
    }
}


// WIFI事件处理函数
static void wifiEventHandler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data){
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
        esp_wifi_connect();
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
        if (s_retry_num < WIFI_MAXIMUM_RETRY) {
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGI(TAG, "retry to connect to the AP");
        } else {
            xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
        }
        ESP_LOGI(TAG,"connect to the AP fail");
    } else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        s_retry_num = 0;
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    }
}


void wifiStart(void){
    s_wifi_event_group = xEventGroupCreate();
    
    ESP_ERROR_CHECK(esp_netif_init());
    
    esp_netif_create_default_wifi_sta();
    
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    // 创建esp事件处理实例
    esp_event_handler_instance_t instance_any_id;
    esp_event_handler_instance_t instance_got_ip;

    // 注册esp事件实例
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &wifiEventHandler,
                                                        NULL,
                                                        &instance_any_id));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                        IP_EVENT_STA_GOT_IP,
                                                        &wifiEventHandler,
                                                        NULL,
                                                        &instance_got_ip));

    wifi_config_t wifi_config = {
        .sta = {
            .ssid = WIFI_SSID,
            .password = WIFI_PASS,
            /* Setting a password implies station will connect to all security modes including WEP/WPA.
             * However these modes are deprecated and not advisable to be used. Incase your Access point
             * doesn't support WPA2, these mode can be enabled by commenting below line */
	     .threshold.authmode = WIFI_AUTH_WPA2_PSK,

            .pmf_cfg = {
                .capable = true,
                .required = false
            },
        },
    };
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start() );

    ESP_LOGI(TAG, "wifi_init_sta finished.");

    /* Waiting until either the connection is established (WIFI_CONNECTED_BIT) or connection failed for the maximum
     * number of re-tries (WIFI_FAIL_BIT). The bits are set by event_handler() (see above) */
    EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
            WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
            pdFALSE,
            pdFALSE,
            portMAX_DELAY);

    /* xEventGroupWaitBits() returns the bits before the call returned, hence we can test which event actually
     * happened. */
    if (bits & WIFI_CONNECTED_BIT) {
        ESP_LOGI(TAG, "connected to ap SSID:%s password:%s",
                 WIFI_SSID, WIFI_PASS);
    } else if (bits & WIFI_FAIL_BIT) {
        ESP_LOGI(TAG, "Failed to connect to SSID:%s, password:%s",
                 WIFI_SSID, WIFI_PASS);
    } else {
        ESP_LOGE(TAG, "UNEXPECTED EVENT");
    }

    /* The event will not be processed after unregister */
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, instance_got_ip));
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(WIFI_EVENT, ESP_EVENT_ANY_ID, instance_any_id));
    vEventGroupDelete(s_wifi_event_group);
    ESP_LOGI(TAG, "s_wifi_event_group was deleted.");
}


static esp_err_t mqttEventHandlerCb(esp_mqtt_event_handle_t event){
    esp_mqtt_client_handle_t client = event->client;
    int msg_id;
    // your_context_t *context = event->context;
    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
            msg_id = esp_mqtt_client_publish(client, "/topic/qos1", "data_3", 0, QOS1, 0);
            ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);

            msg_id = esp_mqtt_client_subscribe(client, MQTT_TOPIC_ISLETSPACE_IOT_CTRL, QOS0);  //ESP32客户端会一直订阅这个频道以接收控制信息

            ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);

            msg_id = esp_mqtt_client_subscribe(client, "/topic/qos1", QOS1);
            ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);

            msg_id = esp_mqtt_client_unsubscribe(client, "/topic/qos1");
            ESP_LOGI(TAG, "sent unsubscribe successful, msg_id=%d", msg_id);
            break;
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
            break;

        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);

            // 启动时，ESP32客户端会发布的状态信息至服务器
            msg_id = esp_mqtt_client_publish(client, MQTT_TOPIC_ISLETSPACE_IOT_STATUS, MQTT_INIT_MSG_ISLETSPACE_IOT, 0, QOS0, 0);    

            ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_DATA:  //如果接收到数据

            // 如果数据长度与第一个数据内容符合要求，则进行显示
            if((event->data_len <= 10) && (event->data[0]=='i')){
                ESP_LOGI(TAG, "MQTT_EVENT_DATA");
                printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
                printf("DATA=%.*s\r\n", event->data_len, event->data);

                // GPIO_ISLET_HEAT 控制
                if(event->data[gpio_heat] == '1'){
                    gpio_set_level(GPIO_ISLET_HEAT, HIGH_LEVEL);
                }
                else{
                    gpio_set_level(GPIO_ISLET_HEAT, LOW_LEVEL);
                }


                // GPIO_ISLET_PUMP 控制
                if(event->data[gpio_pump] == '1'){
                    gpio_set_level(GPIO_ISLET_PUMP, HIGH_LEVEL);
                }
                else{
                    gpio_set_level(GPIO_ISLET_PUMP, LOW_LEVEL);
                }


                // GPIO_ISLET_LIGHT 控制
                if(event->data[gpio_light] == '1'){
                    gpio_set_level(GPIO_ISLET_LIGHT, HIGH_LEVEL);
                }
                else{
                    gpio_set_level(GPIO_ISLET_LIGHT, LOW_LEVEL);
                }

                // 发布最新温度至服务器
                // if(DHT11ReadTemHum(DHT11Data, GPIO_ISLET_TEMP1)){
                //     temperature = DHT11Data[2];
                //     humidity = DHT11Data[0];
                //     printf("temp = %d, humidity = %d ", temperature, humidity);

                //     // msg_id = esp_mqtt_client_publish(client, MQTT_TOPIC_ISLETSPACE_IOT_STATUS, MQTT_INIT_MSG_ISLETSPACE_IOT, 0, QOS0, 0);    
                // }

            }


            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
            if (event->error_handle->error_type == MQTT_ERROR_TYPE_TCP_TRANSPORT) {
                logErrorIfNonzero("reported from esp-tls", event->error_handle->esp_tls_last_esp_err);
                logErrorIfNonzero("reported from tls stack", event->error_handle->esp_tls_stack_err);
                logErrorIfNonzero("captured as transport's socket errno",  event->error_handle->esp_transport_sock_errno);
                ESP_LOGI(TAG, "Last errno string (%s)", strerror(event->error_handle->esp_transport_sock_errno));
            }
            break;
        default:
            ESP_LOGI(TAG, "Other event id:%d", event->event_id);
            break;
    }
    return ESP_OK;
}


static void mqttEventHandler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
    ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    mqttEventHandlerCb(event_data);
}


static void mqttStart(void){
    esp_mqtt_client_config_t mqtt_cfg = {
        .uri = CONFIG_BROKER_URL,
    };
#if CONFIG_BROKER_URL_FROM_STDIN
    char line[128];

    if (strcmp(mqtt_cfg.uri, "FROM_STDIN") == 0) {
        int count = 0;
        printf("Please enter url of mqtt broker\n");
        while (count < 128) {
            int c = fgetc(stdin);
            if (c == '\n') {
                line[count] = '\0';
                break;
            } else if (c > 0 && c < 127) {
                line[count] = c;
                ++count;
            }
            vTaskDelay(10 / portTICK_PERIOD_MS);
        }
        mqtt_cfg.uri = line;
        printf("Broker url: %s\n", line);
    } else {
        ESP_LOGE(TAG, "Configuration mismatch: wrong broker url");
        abort();
    }
#endif /* CONFIG_BROKER_URL_FROM_STDIN */

    esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqttEventHandler, client);
    esp_mqtt_client_start(client);
}


static void gpioInit(){

    // 对 GPIO_ISLET_HEAT / GPIO_ISLET_PUMP / GPIO_ISLET_LIGHT 进行控制
    // 设置为输出模式，并关闭置高置低。
    gpio_config_t io_conf;                         // 创建配置对象（仅数据）
    io_conf.intr_type = GPIO_INTR_DISABLE;         // 关闭GPIO中断
    io_conf.mode = GPIO_MODE_OUTPUT;               // 设置为输出模式
    io_conf.pin_bit_mask = GPIO_ISLET_OUTPUT;      // 配置需要作为输出的GPIO（掩码）
    io_conf.pull_down_en = DISABLE;                // 关闭置低模式
    io_conf.pull_up_en = DISABLE;                  // 关闭置高模式
    gpio_config(&io_conf);                         // 配置GPIO数据
    ESP_LOGI(TAG, "GPIO OUTPUT PINS ARE CONFIGURED!");


    io_conf.intr_type= GPIO_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pin_bit_mask = GPIO_ISLET_INPUT;
    io_conf.pull_up_en = ENABLE;
    gpio_config(&io_conf);
    ESP_LOGI(TAG, "GPIO INPUT PINS ARE CONFIGURED!");
}


void DelayUs(uint32_t nCount){
    ets_delay_us(nCount);
}


void dht11Start(void){
//     dht11Init(GPIO_ISLET_TEMP1);
//     dht11Init(GPIO_ISLET_TEMP2);
}


void dht11Init(int pin){ 
    DHT11_OUT(pin);      //设置端口方向
    DHT11_CLR(pin);      //拉低端口  
    DelayUs(19*1000);
    
    DHT11_SET((pin));      //释放总线
    DelayUs(30);                        //总线由上拉电阻拉高，主机延时30uS;
    DHT11_IN((pin));       //设置端口方向

    while(!gpio_get_level((pin)));   //DHT11等待80us低电平响应信号结束
    while(gpio_get_level((pin)));    //DHT11将总线拉高80us

    ESP_LOGI(TAG,"in function dht11Init(),pin %d is configured.",pin);
}


uint8_t DHT11ReadValue(int pin){ 
    uint8_t i,sbuf=0;
    for(i=8;i>0;i--){
        sbuf<<=1; 
        while(!gpio_get_level(pin));
        DelayUs(30);                        // 延时 30us 后检测数据线是否还是高电平 
        if(gpio_get_level(pin)){
            sbuf|=1;  
        }
        else{
            sbuf|=0;
        }
        while(gpio_get_level(pin));
    }
    return sbuf;   
}


uint8_t DHT11ReadTemHum(uint8_t *buf,int pin){
    uint8_t check;
    buf[0] = DHT11ReadValue(pin);
    buf[1] = DHT11ReadValue(pin);
    buf[2] = DHT11ReadValue(pin);
    buf[3] = DHT11ReadValue(pin);
    
    check = DHT11ReadValue(pin);

    if(check == buf[0]+buf[1]+buf[2]+buf[3]) return 1;
    else return 0;
} 






